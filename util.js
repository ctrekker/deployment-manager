import mongoose from 'mongoose'

export function mongooseModel(name, schema) {
  let model
  try {
    model = mongoose.model(name)
  }
  catch(err) {
    model = mongoose.model(name, schema);
  }
  return model
}

export function fetcher(url) {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(response => response.json())
      .then(result => resolve(result))
      .catch(reject);
  });
}
