import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';

function Navbar(props) {
  return (
    <AppBar position={'static'}>
      <Toolbar>
        <Typography variant={'h5'}>Deployment Manager</Typography>
      </Toolbar>
    </AppBar>
  );
}

export default Navbar;
