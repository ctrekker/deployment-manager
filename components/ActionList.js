import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';

function ActionList({ entries, actionIcon, onAction }) {
  const listEntries = [];
  for(let i=0; i<entries.length; i++) {
    const entry = entries[i];
    listEntries.push(
      <ListItem key={i}>
        <ListItemText primary={entry.primary} secondary={entry.secondary}/>
        { actionIcon &&
        <ListItemSecondaryAction>
          <IconButton edge={'end'} onClick={(e) => onAction(e, i)}>{actionIcon}</IconButton>
        </ListItemSecondaryAction>
        }
      </ListItem>
    )
  }
  
  return (
    <List>
      {listEntries}
    </List>
  )
}

export default ActionList;
