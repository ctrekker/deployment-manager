import mongoose from 'mongoose'

const withDatabase = handler => async (req, res) => {
  if (!mongoose.connection.readyState) {
    const uri = 'mongodb://depman:IBiB22KAdFrHR89d1fJo@192.168.1.18:27017/depman?authSource=admin'
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      autoIndex: false
    })
  }
  
  return handler(req, res)
}

export default withDatabase