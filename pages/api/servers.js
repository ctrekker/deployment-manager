import Server from '../../models/server';
import withDatabase from '../../middleware/database';

const handler = async (req, res) => {
  if(req.method === 'GET') {
    console.log(Server);
    res.json(await Server.find({}));
  }
  else if(req.method === 'POST') {
    res.json(await Server.create(req.body));
  }
  else if(req.method === 'DELETE')  {
    console.log(req.body);
    res.json(await Server.deleteOne({ _id: req.body._id }));
  }
}

export default withDatabase(handler);
