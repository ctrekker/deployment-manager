import Head from 'next/head'
import React, { useRef, useState } from 'react';
import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Navbar from '../components/Navbar';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import useSWR, { mutate } from 'swr';
import Alert from '@material-ui/lab/Alert';
import CircularProgress from '@material-ui/core/CircularProgress';
import { fetcher } from '../util';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ActionList from '../components/ActionList';

export default function Home() {
  const { data: servers, error: serversError } = useSWR('/api/servers', fetcher);
  const [ createServer, setCreateServer ] = useState(false);
  
  const serverName = useRef();
  const serverHost = useRef();
  
  
  function handleServerCreate(e) {
    fetch('/api/servers', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: serverName.current.value,
        host: serverHost.current.value
      })
    })
      .then(res => mutate('/api/servers'))
      .catch(err => console.log(err));
  }
  function handleServerDelete(e, serverId) {
    fetch('/api/servers', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        _id: servers[serverId]._id
      })
    })
      .then(res => mutate('/api/servers'))
      .catch(err => console.log(err));
  }
  
  
  return (
    <React.Fragment>
      <Navbar/>
      <div className="container">
        <Head>
          <title>Deployment Manager</title>
        </Head>
    
        <Container maxWidth={'sm'}>
          <Paper elevation={3}>
            <div className={'container'}>
              <div className={'flexCenter'}>
                <Typography variant={'h5'}>Deployments</Typography>
                <IconButton><AddIcon/></IconButton>
              </div>
            </div>
            <div className={'container'}>
              <div className={'flexCenter'}>
                <Typography variant={'h5'}>Servers</Typography>
                <IconButton onClick={() => setCreateServer(true)}><AddIcon/></IconButton>
              </div>
              <Collapse in={createServer}>
                <div style={{ padding: '15px' }}>
                  <TextField className={'gutterBottom'} inputRef={serverName} variant={'outlined'} label={'Name'} fullWidth/>
                  <TextField className={'gutterBottom'} inputRef={serverHost} variant={'outlined'} label={'Host'} fullWidth/>
                  <div className={'flexCenter'}>
                    <div style={{ flexGrow: 1 }}/>
                    <Button variant={'contained'} onClick={() => setCreateServer(false)} style={{ marginRight: '7px' }}>Cancel</Button>
                    <Button variant={'contained'} color={'primary'} onClick={handleServerCreate}>Create</Button>
                  </div>
                </div>
              </Collapse>
              <div>
                { serversError && <Alert severity={'error'}>Servers could not be fetched</Alert>}
                { !serversError && !servers && <CircularProgress/> }
                { servers &&
                  <ActionList
                    entries={servers.map(x => ({
                      primary: x.name,
                      secondary: x.host,
                    }))}
                    actionIcon={<DeleteIcon/>}
                    onAction={handleServerDelete}/> }
              </div>
            </div>
          </Paper>
        </Container>
        
    
        <style jsx global>{`
        html, body {
          margin: 0; padding: 0;
        }
        .flexCenter {
          display: flex;
          align-items: center;
        }
        .gutterBottom {
          margin-bottom: 10px !important;
        }
        .container {
          margin: 15px;
        }
        `}</style>
      </div>
    </React.Fragment>
  )
}
