import mongoose from 'mongoose'
import { mongooseModel } from '../util';

const ServerSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  host: {
    type: String,
    unique: true,
    required: true
  }
});

const Server = mongooseModel('Server', ServerSchema);

export default Server;
